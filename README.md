# Distinct elements counter

The tool for counting distinct elements of a collection, that cannot fully be stored in memory, represented by its iterator.

Note that ```null``` elements consider as usual elements.
That means that if collection contains null elements then the result will be amount of distinct not-null elements plus 1 (as all null elements consider equals).
 
## Getting started

### Requisites

In order to use application you need following:

* Java 8 (or later)
* Maven

### Compilation

Download project. Go to folder and run following command in the command line:

```
mvn clean install
```

It will run tests and compile project.

## Main idea of the project

Implementation based on divide and conquer algorithm design used in merge sort algorithm.

On the first step save elements of input in buckets with maximum size of **maxElementsInMemory** 
and containing only distinct elements in ascending order.

On next step merge **maxElementsInMemory** buckets into new one that meets the following conditions: all elements are distinct and they rest in bucket in ascending order.

Repeat previous step until one bucket left. This bucket will contain all distinct elements of input in ascending order. Amount of distinct elements of input is the size of the last bucket