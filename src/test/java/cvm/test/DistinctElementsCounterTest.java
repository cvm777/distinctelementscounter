package cvm.test;

import cvm.test.impl.DistinctElementsCounterImpl;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link DistinctElementsCounter} implemented by {@link DistinctElementsCounterImpl} class.
 * 
 */
public class DistinctElementsCounterTest {

    private static final Random RND = new Random();

    /**
     * Test case when all elements could be stored in memory.
     * 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    @Test
    public void allInMemoryIntegerTest() throws IOException, ClassNotFoundException {
        List<Integer> randomIntegers = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            randomIntegers.add(RND.nextInt(5000));
        }
        DistinctElementsCounter<Integer> counter = new DistinctElementsCounterImpl<>(5000);
        assertEquals((new TreeSet<>(randomIntegers)).size(), counter.count(randomIntegers.iterator()));
    }

    /**
     * Regular test case.
     * 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    @Test
    public void randomIntegerTest() throws IOException, ClassNotFoundException {
        List<Integer> randomIntegers = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            randomIntegers.add(RND.nextInt());
        }
        DistinctElementsCounter<Integer> counter = new DistinctElementsCounterImpl<>(20);
        assertEquals((new TreeSet<>(randomIntegers)).size(), counter.count(randomIntegers.iterator()));
    }

    /**
     * Test case where all elements are string and elements go around in circle
     * 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    @Test
    public void repeatableStringsTest() throws IOException, ClassNotFoundException {
        List<String> repeatableRandomStrings = new ArrayList<>();
        for (int i = 0; i < 300; i++) {
            repeatableRandomStrings.add(generateRandomString(RND.nextInt(100) + 1));
        }
        while (repeatableRandomStrings.size() < 1000) {
            repeatableRandomStrings.add(repeatableRandomStrings.get(repeatableRandomStrings.size() % 300));
        }
        DistinctElementsCounter<String> counter = new DistinctElementsCounterImpl<>(2 + RND.nextInt(19));
        assertEquals((new TreeSet<>(repeatableRandomStrings)).size(), counter.count(repeatableRandomStrings.iterator()));
    }
    
    /**
     * Test for null iterator was transfered to method
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws NullPointerException 
     */
    @Test(expected = NullPointerException.class)
    public void nullIteratorTest() throws IOException, ClassNotFoundException, NullPointerException {
        DistinctElementsCounter<String> counter = new DistinctElementsCounterImpl<>();
        counter.count(null);
    }
    
    /**
     * Test case when collection contains {@code null} elements.
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    @Test
    public void integersAndNullsTest() throws IOException, ClassNotFoundException {
        List<Integer> randomIntegers = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            randomIntegers.add(RND.nextInt());
        }
        for (int i = 0; i < 100; i++) {
            randomIntegers.set(RND.nextInt(10000), null);
        }
        TreeSet<Integer> result = new TreeSet<>();
        randomIntegers.forEach(value -> {
            if (value != null) {
                result.add(value);
            }
        });
        DistinctElementsCounter<Integer> counter = new DistinctElementsCounterImpl<>(20);
        assertEquals(result.size() + 1, counter.count(randomIntegers.iterator()));
    }

    /**
     * Test for creating {@link DistinctElementsCounterImpl} with maximum number of elements in memory less than {@code 2}
     * 
     * @throws IllegalArgumentException 
     */
    @Test(expected = IllegalArgumentException.class)
    public void illegalArgumentExceptionTest() throws IllegalArgumentException {
        new DistinctElementsCounterImpl<>(-1);
    }

    /**
     * Generate random {@link String} of length {@code len}
     * 
     * @param len
     * @return generated String
     */
    private String generateRandomString(int len) {
        StringBuilder builder = new StringBuilder();
        while (len-- > 0) {
            builder.append((char)RND.nextInt((1<<16)));
        }
        return builder.toString();
    }

}
