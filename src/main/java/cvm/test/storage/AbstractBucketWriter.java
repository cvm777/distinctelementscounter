package cvm.test.storage;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;

/**
 * This class provide methods that each bucket writer should implement depend on its type
 * 
 * @param <T> the type of elements maintained by this writer. Must be {@link Serializable}
 */
public abstract class AbstractBucketWriter<T extends Serializable> implements Closeable {

    /**
     * Pushes {@code element} to the end of bucket.
     * @param element Element to push
     * @throws IOException 
     */
    public abstract void push(T element) throws IOException;

    /**
     * Returns current number of elements in the bucket.
     * @return Current number of elements in the bucket
     */
    public abstract int size();

}
