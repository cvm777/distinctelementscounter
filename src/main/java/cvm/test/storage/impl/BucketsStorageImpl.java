package cvm.test.storage.impl;

import cvm.test.storage.AbstractBucketWriter;
import cvm.test.storage.BucketAlreadyExistsException;
import cvm.test.storage.BucketNotExistsException;
import cvm.test.storage.BucketsStorage;

import java.io.*;
import java.util.*;

/**
 * Implementation of {@link BucketsStorage} based on serialization of object into file.
 * 
 * Each file 
 * 
 * @author root
 * @param <T> 
 */
public class BucketsStorageImpl<T extends Serializable> implements BucketsStorage<T> {

    private static final String BUCKET_PREFIX = "bucket_";
    private static final String BUCKET_LVL_ID_SEPARATOR = "_";

    /**
     * Iterator for a bucket saved in file.
     * 
     * <p>Implements {@link Iterator} for iterating all elements within a given bucket. The end of file determines by {@code null} object.</p>
     * 
     * <p>Use {@link ObjectInputStream} to read object from file.</p>
     * 
     * @param <T> 
     */
    private class BucketIterator<T> implements Iterator<T> {

        private final int bucketLvl;
        private final int bucketId;
        private T lastObject;
        private boolean hasNext;
        private ObjectInputStream ois;

        /**
         * Constructs the iterator for the bucket.
         * 
         * If file for bucket does not exists the {@link BucketNotExistsException} will be thrown.
         * 
         * @param bucketLvl level on which bucket located
         * @param bucketId id of bucket in this level
         * @throws IOException
         * @throws ClassNotFoundException
         * @throws BucketNotExistsException in case of file for the bucket does not exists.
         */
        private BucketIterator(int bucketLvl, int bucketId) throws IOException, ClassNotFoundException, BucketNotExistsException {
            this.bucketLvl = bucketLvl;
            this.bucketId = bucketId;
            final File bucketFile = new File(dataDirectory.getAbsolutePath() + File.separator + BUCKET_PREFIX + bucketLvl + BUCKET_LVL_ID_SEPARATOR + bucketId);
            if (!bucketFile.exists()) {
                throw new BucketNotExistsException("Bucket #" + bucketId + " on level #" + bucketLvl + " not exists");
            }
            ois = new ObjectInputStream(new FileInputStream(new RandomAccessFile(bucketFile, "r").getFD()));
            lastObject = (T) ois.readObject();
            hasNext = lastObject != null;
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public T next() {
            if (hasNext) {
                T result = lastObject;
                try {
                    lastObject = (T) ois.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException("Cannot read object", e);
                }
                hasNext = lastObject != null;
                if (!hasNext) {
                    try {
                        ois.close();
                    } catch (IOException e) {
                        throw new RuntimeException("Cannot close ObjectInputStream", e);
                    }
                }
                return result;
            }
            throw new NoSuchElementException();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BucketIterator<?> that = (BucketIterator<?>) o;
            return bucketLvl == that.bucketLvl &&
                    bucketId == that.bucketId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(bucketLvl, bucketId);
        }
    }

    /**
     * Implementation of {@link AbstractBucketWriter} which write not null elements to file associated with the bucket.
     * 
     * The last written element always equals to {@code null}. In case of attempt to add {@code null} object the {@link NullPointerException} will be thrown.
     * 
     * @param <T> 
     */
    private class BucketWriter<T extends Serializable> extends AbstractBucketWriter<T> {

        private ObjectOutputStream oos;
        private int size;

        /**
         * Constructs the {@link BucketWriter} for bucket #{@code bucketLvl} on level #{@code bucketId}.
         * 
         * If file already exists the {@link BucketAlreadyExistsException} will be thrown.
         * 
         * @param bucketLvl
         * @param bucketId
         * @throws IOException
         * @throws BucketAlreadyExistsException in case of file for the bucket already exists
         * @throws NullPointerException
         */
        public BucketWriter(int bucketLvl, int bucketId) throws IOException, BucketAlreadyExistsException {
            final File bucketFile = new File(dataDirectory.getAbsolutePath() + File.separator + BUCKET_PREFIX + bucketLvl + BUCKET_LVL_ID_SEPARATOR + bucketId);
            if (bucketFile.exists()) {
                throw new BucketAlreadyExistsException("Bucket #" + bucketId + " on level #" + bucketLvl + " already exists");
            }
            oos = new ObjectOutputStream(new FileOutputStream(new RandomAccessFile(bucketFile, "rw").getFD()));
            size = 0;
        }

        @Override
        public void push(T element) throws IOException {
            if (element == null) {
                throw new NullPointerException("This type of bucket storage cannot store null elements.");
            }
            oos.writeObject(element);
            oos.flush();
            size++;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public void close() throws IOException {
            oos.writeObject(null);
            oos.close();
        }
    }

    private final File dataDirectory;

    /**
     * Constructs the {@link BucketsStorageImpl}.
     * 
     * Creates not existing temporary directory for bucket's files.
     */
    public BucketsStorageImpl() {
        File tempDataDirectory = new File(UUID.randomUUID().toString());
        while (tempDataDirectory.exists()) {
            tempDataDirectory = new File(UUID.randomUUID().toString());
        }
        this.dataDirectory = tempDataDirectory;
        tempDataDirectory.mkdirs();
    }

    /**
     * Save the data into bucket #{@code bucketId} on level #{@code bucketLvl}.
     * 
     * The last written element always equals to {@code null}. In case of collection contains {@code null} object the {@link NullPointerException} will be thrown.
     * 
     * @param bucketLvl
     * @param bucketId
     * @param data
     * @throws IOException
     * @throws BucketAlreadyExistsException 
     * @throws NullPointerException
     */
    @Override
    public void store(int bucketLvl, int bucketId, Collection<T> data) throws IOException, BucketAlreadyExistsException {
        final File bucketFile = new File(dataDirectory.getAbsolutePath() + File.separator + BUCKET_PREFIX + bucketLvl + BUCKET_LVL_ID_SEPARATOR + bucketId);
        if (bucketFile.exists()) {
            throw new BucketAlreadyExistsException("Bucket #" + bucketId + " on level #" + bucketLvl + " already exists");
        }
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new RandomAccessFile(bucketFile, "rw").getFD()))) {
            for (T dataElement : data) {
                if (dataElement == null) {
                    throw new NullPointerException("This type of bucket storage cannot store null elements.");
                }
                oos.writeObject(dataElement);
                oos.flush();
            }
            oos.writeObject(null);
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Provide object of {@link BucketWriter} class for the bucket #{@code bucketId} on level #{bucketLvl}.
     * 
     * @param bucketLvl
     * @param bucketId
     * @return
     * @throws IOException
     * @throws BucketAlreadyExistsException 
     */
    @Override
    public AbstractBucketWriter<T> createBucketWriter(int bucketLvl, int bucketId) throws IOException, BucketAlreadyExistsException {
        return new BucketWriter<>(bucketLvl, bucketId);
    }

    /**
     * Provide object of {@link BucketIterator} class for the bucket #{@code bucketId} on level #{bucketLvl}.
     * 
     * @param bucketLvl
     * @param bucketId
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws BucketNotExistsException 
     */
    @Override
    public Iterator<T> getBucketIterator(int bucketLvl, int bucketId) throws IOException, ClassNotFoundException, BucketNotExistsException {
        return new BucketIterator<>(bucketLvl, bucketId);
    }

    /**
     * Delete all files from temporary created directory.
     * 
     * @throws SecurityException if the underlying method {@code  delete} of {@link File} thrown it
     */
    private void clearDataDirectory() {
        for (File file : dataDirectory.listFiles()) {
            file.delete();
        }
        dataDirectory.delete();
    }

    /**
     * Cleans up all temporary data from disk.
     */
    @Override
    public void close() {
        clearDataDirectory();
    }

}
