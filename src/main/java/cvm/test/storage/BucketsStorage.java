package cvm.test.storage;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * Interface that describes the bucket storage must implement
 * 
 * @param <T> the type of elements maintained by this counter. Must be {@link Serializable}
 */
public interface BucketsStorage<T extends Serializable> extends Closeable {

    /**
     * Store the {@code data} in bucket #{@code bucketId} on level #{@code bucketLvl}
     * @param bucketLvl level on which bucket must be stored
     * @param bucketId id of bucket in this level
     * @param data collection of elements that must be stored in bucket
     * @throws IOException
     * @throws BucketAlreadyExistsException 
     */
    void store(int bucketLvl, int bucketId, Collection<T> data) throws IOException, BucketAlreadyExistsException;

    /**
     * Creates object of subclass {@link AbstractBucketWriter} that will write data into bucket #{@code bucketId} on level #{@code bucketLvl}
     * @param bucketLvl level on which bucket located
     * @param bucketId id of bucket in this level
     * @return The writer for bucket #{@code bucketId} on level #{@code bucketLvl}
     * @throws IOException
     * @throws BucketAlreadyExistsException 
     */
    AbstractBucketWriter<T> createBucketWriter(int bucketLvl, int bucketId) throws IOException, BucketAlreadyExistsException;

    /**
     * Creates iterator for bucket #{@code bucketId} on level #{@code bucketLvl}
     * @param bucketLvl level on which bucket located
     * @param bucketId id of bucket in this level
     * @return Iterator 
     * @throws IOException
     * @throws ClassNotFoundException 
     * @throws BucketNotExistsException 
     */
    Iterator<T> getBucketIterator(int bucketLvl, int bucketId) throws IOException, ClassNotFoundException, BucketNotExistsException;

}
