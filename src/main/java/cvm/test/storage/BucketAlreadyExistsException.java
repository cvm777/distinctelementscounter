package cvm.test.storage;

/**
 * Exception indicates that the bucket which is tries to be created already exists in {@link BucketsStorage}.
 * 
 */
public class BucketAlreadyExistsException extends Exception {

    public BucketAlreadyExistsException() {
    }

    public BucketAlreadyExistsException(String message) {
        super(message);
    }

    public BucketAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public BucketAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
