package cvm.test.storage;
/**
 * Exception indicates that bucket does not exists in {@link BucketsStorage}.
 * 
 */
public class BucketNotExistsException extends Exception {

    public BucketNotExistsException() {
    }

    public BucketNotExistsException(String message) {
        super(message);
    }

    public BucketNotExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public BucketNotExistsException(Throwable cause) {
        super(cause);
    }

    public BucketNotExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
