package cvm.test.impl;

import cvm.test.DistinctElementsCounter;
import cvm.test.storage.BucketAlreadyExistsException;
import cvm.test.storage.AbstractBucketWriter;
import cvm.test.storage.BucketNotExistsException;
import cvm.test.storage.BucketsStorage;
import cvm.test.storage.impl.BucketsStorageImpl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

/**
 * A {@link DistinctElementsCounter} implementation based on divide and conquer algorithm design used in merge sort algorithm.
 * 
 * <p>On the first step save elements of input in buckets with maximum size of <b>maxElementsInMemory</b> 
 * and containing only distinct elements in ascending order.
 * On next step merge <b>maxElementsInMemory</b> buckets into new one that meets the following conditions: all elements are distinct and they rest in bucket in ascending order. <br/>
 * Repeat previous step until one bucket left. This bucket will contain all distinct elements of input in ascending order. Amount of distinct elements of input is the size of the last bucket<br/>
 * </p>
 * 
 * 
 * @param <T> the type of elements maintained by this counter. Must be {@link Comparable} and {@link Serializable}
 */
public class DistinctElementsCounterImpl<T extends Comparable<T> & Serializable> implements DistinctElementsCounter<T> {

    /**
     * Private inner class for linking object and bucket that contains this object
     */
    private class ObjectBucket implements Comparable<ObjectBucket> {
        T object;
        final int bucketId;

        public ObjectBucket(T object, int bucketId) {
            this.object = object;
            this.bucketId = bucketId;
        }

        @Override
        public int compareTo(ObjectBucket o) {
            return object.compareTo(o.object);
        }
    }

    private final int maxElementsInMemory;

    /**
     * Constructs a new DistinctElementCounterImpl with specified parameter of maximum amount of elements that could be stored in memory.
     * @param maxElementsInMemory maximum amount elements that could be stored in memory
     */
    public DistinctElementsCounterImpl(int maxElementsInMemory) {
        if (maxElementsInMemory <= 1) {
            throw new IllegalArgumentException("MaxElementsInMemory must be positive integer greater than 1");
        }
        this.maxElementsInMemory = maxElementsInMemory;
    }
    
    /**
     * Constructs a new DistinctElementCounterImpl with default maximum amount of elements that could be stored in memory.
     * 
     * The default maximum amount of elements that could be stored in memory is equals 2
     */
    public DistinctElementsCounterImpl() {
        this(2);
    }

    /**
     * Merge buckets staring from level 0 that contains {@code bucketsCount} buckets.
     * 
     * <p>Merge buckets until one bucket left. Each bucket will contains distinct elements in ascending order.
     * Return size of this bucket.</p>
     * 
     * @param bucketsStorage storage object for writing/reading buckets
     * @param bucketsCount amount of buckets after first step of algorithm
     * @return number of elements in last bucket
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    private int merge(BucketsStorage<T> bucketsStorage, int bucketsCount) throws IOException, ClassNotFoundException {
        TreeSet<ObjectBucket> bucket = new TreeSet<>();
        for (int lvl = 0; ; lvl++) {
            int newBucketsCount = 0;
            int result = 0;
            for (int leftBucket = 0; leftBucket < bucketsCount; ) {
                int rightBucket = Math.min(bucketsCount, leftBucket + maxElementsInMemory);
                HashMap<Integer, Iterator<T>> aliveIterators = new HashMap<>(rightBucket - leftBucket);
                for (int i = leftBucket; i < rightBucket; i++) {
                    try {
                        Iterator<T> bucketIterator = bucketsStorage.getBucketIterator(lvl, i);
                        while (bucketIterator.hasNext()) {
                            if (bucket.add(new ObjectBucket(bucketIterator.next(), i))) {
                                break;
                            }
                        }
                        if (bucketIterator.hasNext()) {
                            aliveIterators.put(i, bucketIterator);
                        }
                    } catch (BucketNotExistsException e) {
                        throw new RuntimeException("", e);
                    }
                }
                try (AbstractBucketWriter<T> bucketWriter = bucketsStorage.createBucketWriter(lvl + 1, newBucketsCount++)) {
                    while (!aliveIterators.isEmpty()) {
                        ObjectBucket first = bucket.pollFirst();
                        bucketWriter.push(first.object);
                        if (aliveIterators.containsKey(first.bucketId)) {
                            Iterator<T> it = aliveIterators.get(first.bucketId);
                            first.object = it.next();
                            while (!bucket.add(first) && it.hasNext()) {
                                first.object = it.next();
                            }
                            if (!it.hasNext()) {
                                aliveIterators.remove(first.bucketId);
                            }
                        }
                    }
                    while (!bucket.isEmpty()) {
                        bucketWriter.push(bucket.pollFirst().object);
                    }
                    result += bucketWriter.size();
                } catch (BucketAlreadyExistsException e) {
                    throw new RuntimeException("", e);
                }
                leftBucket = rightBucket;
            }
            if (newBucketsCount == 1) {
                return result;
            }
            bucketsCount = newBucketsCount;
        }
    }

    /**
     * Counts distinct elements of the collection of comparable elements represented by its iterator.
     * 
     * Puts not null elements into buckets. Each bucket will contains distinct elements in ascending order. Then run merging of these buckets.
     * 
     * @param iter iterator of a collection
     * @return amount of distinct elements in collection
     * @throws IOException if problems with writing or reading elements occur
     * @throws ClassNotFoundException if problems with de-serialization appear
     * @throws RuntimeException if other problems appears during counting
     */
    @Override
    public int count(Iterator<T> iter) throws IOException, ClassNotFoundException {
        if (Objects.isNull(iter)) {
            throw new NullPointerException("Iterator is null");
        }
        try (BucketsStorage<T> bucketsStorage = new BucketsStorageImpl<>()) {
            boolean nullsExists = false;
            TreeSet<T> bucket = new TreeSet<>();
            int savedBuckets = 0;
            while (iter.hasNext()) {
                T element = iter.next();
                if (element == null) {
                    nullsExists = true;
                    continue;
                }
                if (!bucket.contains(element)) {
                    if (bucket.size() == maxElementsInMemory) {
                        bucketsStorage.store(0, savedBuckets++, bucket);
                        bucket.clear();
                    }
                    bucket.add(element);
                }
            }
            if (savedBuckets == 0) {
                return bucket.size() + (nullsExists ? 1 : 0);
            }
            if (!bucket.isEmpty()) {
                bucketsStorage.store(0, savedBuckets++, bucket);
                bucket.clear();
            }
            return merge(bucketsStorage, savedBuckets) + (nullsExists ? 1 : 0);
        } catch (IOException | ClassNotFoundException e) {
            throw e;
        } catch (BucketAlreadyExistsException e) {
            throw new RuntimeException("", e);
        }
    }

}
