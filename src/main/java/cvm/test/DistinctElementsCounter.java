package cvm.test;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;


/**
 * The tool for counting distinct elements of a collection, that cannot fully be stored in memory, represented by its iterator.
 * 
 * <p>Note that {@code null} elements consider as usual elements.
 * That means that if collection contains null elements then the result will be amount of distinct not-null elements plus 1 (as all null elements consider equals).</p>
 * 
 * @param <T> the type of elements maintained by this counter. Must be {@link Comparable} and {@link Serializable}
 */
public interface DistinctElementsCounter<T extends Comparable<T> & Serializable> {

    /**
     * Counts distinct elements of the collection of comparable elements represented by its iterator
     * @param iter iterator of a collection
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    int count(Iterator<T> iter) throws IOException, ClassNotFoundException;

}
